MODULE
------
Related Node

CREDITS
--------

Developed by Abhijeet Singh Kalsi
<abhijeet [dot] kalsi [at] gmail [dot] com>


DESCRIPTION/FEATURES
--------------------
This module is used to display similar or related node (like article, page,
etc) as block from the current node view.


REQUIREMENTS
------------
 - Drupal 7.0
 - Taxonomy


Related Content display
-----------------------
There is one block available for use. The name is "related_node". Configure
  this block as follows

* "Field name to collect the terms" is intended if you have multiple fields
  that uses the field type
* "Item count" is the range of items to display
* "Content type " if you only want to display specific content types for
  related node.

 
INSTALLATION
------------

Decompress the related_node.tar.gz file into your Drupal modules
directory (usually sites/all/modules).

Enable the Related Node : Administration > Modules (admin/modules)
This module requires Media module to be pre installed.


Links
-----
Related Node configuration: admin/config/user-interface/related_node
Block configuration: admin/structure/block
